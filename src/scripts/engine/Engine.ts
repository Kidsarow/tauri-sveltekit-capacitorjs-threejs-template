import * as THREE from 'three';
import type { PerspectiveCamera } from '$scripts/camera/PerspectiveCamera';
import type { Scene } from '$scripts/scene/Scene';

interface EngineOptions {
	/**
	 * @param {number} antialias - Default is false.
	 */
	antialias?: boolean | undefined;
	/**
	 * @param {number} pixelRatio - Default is 1.
	 */
	pixelRatio?: number | undefined;
	/**
	 * @param {string} powerPreference - Can be "high-performance", "low-power" or "default".
	 */
	powerPreference?: string | undefined;
}

/**
 * The Engine will create a renderer using WebGL and a provided scene in a provided canvas with options.
 */
export class Engine {
	/**
	 * The canvas where the WebGL context will be created.
	 */
	private canvas: HTMLCanvasElement;
	/**
	 * The engine's options.
	 */
	private options: EngineOptions;
	/**
	 * The engine's renderer.
	 */
	private renderer: THREE.WebGLRenderer;
	/**
	 * The engine's active scene.
	 */
	private activeScene: Scene | undefined;

	/**
	 * Check if the engine is running a scene.
	 */
	public isRunning: boolean;

	/**
	 * The Engine will create a renderer using WebGL and a provided scene in a provided canvas with options.
	 * @param canvas - The canvas where the WebGL context will be created and renderered.
	 * @param options - The engine's options to be used.
	 */
	constructor(canvas: HTMLCanvasElement, options: EngineOptions) {
		this.canvas = canvas;
		this.options = options;
		this.renderer = new THREE.WebGLRenderer({
			antialias: options.antialias || false,
			canvas: this.canvas,
			powerPreference: options.powerPreference || 'default'
		});
		this.renderer.domElement = this.canvas;
		this.renderer.setPixelRatio(this.options.pixelRatio || 1);
		this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
		this.activeScene = undefined;
		this.isRunning = false;
	}

	/**
	 * Return the active scene.
	 */
	getActiveScene(): Scene | undefined {
		return this.activeScene;
	}

	/**
	 * Resize the renderer.
	 */
	resize(): void {
		this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
	}

	/**
	 * Run the scene in a loop rendering.
	 * @param scene - The scene to be rendered.
	 */
	run(scene?: Scene): void {
		if (scene) {
			this.activeScene = scene;
		} else if (!scene && !this.activeScene) return;
		this.renderer.setAnimationLoop(() => {
			this.renderer.render(
				this.activeScene as Scene,
				this.activeScene?.getActiveCamera() as PerspectiveCamera
			);
		});
		this.isRunning = true;
	}

	/**
	 * Pause the rendering loop process.
	 */
	pause(): void {
		this.renderer.setAnimationLoop(null);
		this.isRunning = false;
	}

	/**
	 * Dispose the scene and the renderer.
	 */
	dispose(): void {
		if (this.activeScene) {
			this.activeScene.traverse((child) => {
				this.activeScene?.remove(child);
			});
		}
		this.renderer.dispose();
	}
}
