import * as THREE from 'three';

/**
 * This projection mode is designed to mimic the way the human eye sees. It is the most common projection mode used for rendering a 3D scene.
 */
class PerspectiveCamera extends THREE.PerspectiveCamera {
	/**
	 * This projection mode is designed to mimic the way the human eye sees. It is the most common projection mode used for rendering a 3D scene.
	 * @param fov - Camera frustum vertical field of view. Default is 50.
	 * @param aspect - Camera frustum aspect ratio. Default is 1.
	 * @param near - Camera frustum near plane. Default is 0.1.
	 * @param far - Camera frustum far plane. Default is 1000.
	 */
	constructor(fov: number = 50, aspect: number = 1, near: number = 0.1, far: number = 1000) {
		super();
		this.fov = fov;
		this.aspect = aspect;
		this.near = near;
		this.far = far;
	}
}

export { PerspectiveCamera };
