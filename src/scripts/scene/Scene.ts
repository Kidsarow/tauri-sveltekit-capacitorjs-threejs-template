import * as THREE from 'three';
import { PerspectiveCamera } from '$scripts/camera/PerspectiveCamera';

/**
 * Scenes allow you to set up what and where is to be rendered. This is where you place objects, lights and cameras.
 */
class Scene extends THREE.Scene {
	/**
	 * The scene's active camera.
	 */
	private activeCamera: PerspectiveCamera | undefined;

	/**
	 * Scenes allow you to set up what and where is to be rendered. This is where you place objects, lights and cameras.
	 */
	constructor() {
		super();
		this.activeCamera = new PerspectiveCamera(50, 1, 0.1, 1000);
		this.add(this.activeCamera);
	}

	/**
	 * Get the active camera.
	 */
	getActiveCamera(): PerspectiveCamera | undefined {
		return this.activeCamera;
	}

	/**
	 * Set the active camera.
	 * @param camera - The camera to be the active scene's camera.
	 */
	setActiveCamera(camera: PerspectiveCamera): void {
		if (!camera) {
			return;
		}
		this.activeCamera = camera;
		this.add(camera);
	}
}

export { Scene };
